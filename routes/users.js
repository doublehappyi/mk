var express = require('express');
var router = express.Router();


function User(name, email) {
    this.name = name
    this.email = email
}

var users = [
    new User('yishuangxi', 'yishuangxi@sina.com'),
    new User('kobe bryant', 'bryant@sina.com'),
    new User('Lebron James', 'lebron@sina.com')
]


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('users', {users: users});
});

module.exports = router;
